let trainer = {
    Name: "Ash Ketchum",
    Age: 10,
    pokemon: [
        "Pikachu",
        "Charizard",
        "Squirtle",
        "Bulbasaur"
    ],
    friends: {
        hoenn: [
            "May",
            "Max",
        ],
        kanto: [
            "Brock",
            "Misty"
        ]
    },
    talkMethod: function(){
        return `${this.pokemon[0]} I choose you!`
    }
}
console.log(trainer.Name);
console.log(trainer.age);
console.log(trainer.pokemon);
console.log(trainer.friends);
console.log(trainer.talkMethod());


function Pokemon(name, level, health, attack){
    this.name = name;
    this.level = level;
    this.health = health * 3;
    this.attack = attack * 1.5;
    this.tackle = function(opponent){
        console.log(`${this.name} used tackle, the health of ${opponent.name} is reduced to ${opponent.health - this.attack}`);
        
    },
    this.faint = function(target){
        console.log(`${target.name} fainted after being attacked by ${this.name}`);
    }
}

let pikachu = new Pokemon("Pikachu", 10, 10, 10);
console.log(pikachu.name);
console.log(pikachu.level);
console.log(pikachu.health);
console.log(pikachu.attack);

let charizard = new Pokemon("Charizard", 20, 20, 20);
console.log(charizard.name);
console.log(charizard.level);
console.log(charizard.health);
console.log(charizard.attack);

let squirtle = new Pokemon("Squirtle", 10, 10, 11);
console.log(squirtle.name);
console.log(squirtle.level);
console.log(squirtle.health);
console.log(squirtle.attack);

squirtle.tackle(pikachu);
squirtle.faint(pikachu);
